import React from 'react'
import { View, Text, TouchableOpacity} from 'react-native'

const Login = ({navigation}) => {
    return (
        <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
            <Text style={{color:'#02006C',fontWeight:'bold', fontSize:40}}>Login</Text>
            <Text style={{marginBottom:15}}>Belum Punya akun?</Text>
            <TouchableOpacity 
                onPress={() => {
                    navigation.navigate('Register')
                }}
            >
                <Text style={{
                    color:'#fff',
                    backgroundColor:'#02006C',
                    paddingHorizontal:50,
                    paddingVertical:10,
                    borderRadius:5,
                    fontSize:20,
                    fontWeight:'700',
                }}>
                    Register
                </Text>
            </TouchableOpacity>
        </View>
    )
}

export default Login
