import React from 'react';
import LottieView from 'lottie-react-native';


const Splash = ({navigation}) => {
    return (
       
            <LottieView
                style={{width:393}}
                source={require('../../../splash.json')}
                autoPlay
                loop={false}
                onAnimationFinish={() => {
                    console.log('finis')
                    navigation.replace('WelcomeAuth')
                }}
            />
    )
}

export default Splash;
