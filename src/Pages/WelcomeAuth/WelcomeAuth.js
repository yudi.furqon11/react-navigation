import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

const WelcomeAuth = ({navigation}) => {
    return (
        <View style={{flex:1,backgroundColor: '#02006C', justifyContent: 'center', alignItems:'center'}}>
            <Text style={{fontWeight: 'bold',marginBottom:100, color:'#fff', fontSize: 60, textAlign:'center' }}>WELCOME</Text>
            <TouchableOpacity 
                onPress={() => {
                    navigation.navigate('Login')
                }}
            >
                <Text style={{
                    color:'#02006C',
                    backgroundColor:'#fff',
                    paddingHorizontal:50,
                    paddingVertical:10,
                    borderRadius:5,
                    fontSize:20,
                    fontWeight:'700',
                }}>
                    Login
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => {
                    navigation.navigate('Home')
                }}
            >
                <Text style={{color:'#fff', fontSize:14,fontWeight:'bold',marginTop:20}}>Lewati</Text>
            </TouchableOpacity>
        </View>
    )
       
        
    
}

export default WelcomeAuth
