import Splash from './Splash/Splash';
import Login from './Login/Login';
import Register from './Register/Register';
import WelcomeAuth from './WelcomeAuth/WelcomeAuth';
import Home from './Home/Home';

export {
    Splash,
    Login,
    Register,
    WelcomeAuth,
    Home
}